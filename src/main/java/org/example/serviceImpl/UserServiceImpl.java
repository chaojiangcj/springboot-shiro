package org.example.serviceImpl;

import org.example.mapper.UserMapper;
import org.example.pojo.User;
import org.example.service.UserService;
import org.example.util.AuthAndRoleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 *@Author: CJ
 *@Date: 2021-10-20 17:46
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserMapper userMapper;

    @Override
    public User selectOne(String username, String password) {
        return userMapper.selectByUser(username, password);
    }

    @Override
    public List<AuthAndRoleDTO> selectAuthAndRole(String username) {
        return userMapper.selectAuthAndRole(username);
    }
}
