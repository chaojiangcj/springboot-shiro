package org.example.util;

/**
 *@Author: CJ
 *@Date: 2021-10-22 1:16
 */
public class AuthAndRoleDTO {

    private String userName;

    private String roleName;

    private String authCode;

    private String authName;

    @Override
    public String toString() {
        return "AuthAndRoleVO{" +
                "userName='" + userName + '\'' +
                ", roleName='" + roleName + '\'' +
                ", authCode='" + authCode + '\'' +
                ", authName='" + authName + '\'' +
                '}';
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getAuthCode() {
        return authCode;
    }

    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    public String getAuthName() {
        return authName;
    }

    public void setAuthName(String authName) {
        this.authName = authName;
    }
}
