package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.example.pojo.Auth;

@Mapper
public interface AuthMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Auth record);

    int insertSelective(Auth record);

    Auth selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Auth record);

    int updateByPrimaryKey(Auth record);
}