package org.example.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.example.pojo.User;
import org.example.util.AuthAndRoleDTO;

import java.util.List;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    User selectByUser(String username, String password);

    List<AuthAndRoleDTO> selectAuthAndRole(String username);
}