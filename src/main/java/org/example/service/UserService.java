package org.example.service;

import org.example.pojo.User;
import org.example.util.AuthAndRoleDTO;

import java.util.List;

/**
 *@Author: CJ
 *@Date: 2021-10-20 17:27
 */
public interface UserService {

    User selectOne(String username, String password);

    List<AuthAndRoleDTO> selectAuthAndRole(String username);
}
