package org.example;

import org.example.service.UserService;
import org.example.util.AuthAndRoleDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * Unit test for simple App.
 */
@RunWith(value = SpringRunner.class)
@SpringBootTest
public class AppTest {

    @Autowired
    private UserService userService;

    @Test
    public void shouldAnswerWithTrue() {
        List<AuthAndRoleDTO> authAndRoleDTOS = userService.selectAuthAndRole("Tom");
        authAndRoleDTOS.forEach(item -> {
            System.out.println(item.getAuthCode());
            System.out.println(item.getAuthName());
        });
    }
}
